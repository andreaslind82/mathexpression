#include "catch.hpp"
#include "../include/MathExpression.h"
#include <vector>
#include <algorithm>
#include <memstat.hpp>

SCENARIO("Creating calculation from VALID input string") {
    GIVEN("a correct input string") {
        MathExpression myExp("(32+5)-(2+1)");

       WHEN("old string is replaced by new VALID string") {
           MathExpression newExp("(100+20)/(20+5)");
           myExp = newExp;

           THEN("string should be valid") {
               REQUIRE(myExp.isValid());

               AND_THEN("returned string should be identical to input string") {
                   REQUIRE(myExp.infixNotation() == "(100+20)/(20+5)");
               }
               AND_THEN("rounding should be correct") {
                   double testVal = double(100+20)/(20+5);
                   Approx target = Approx(testVal).epsilon(0.00);
                   REQUIRE(myExp.calculate() == target);
               }
               AND_THEN("error message should be empty string") {
                   REQUIRE(myExp.errorMessage() == "");
               }
           }
       }
    }
}

SCENARIO("Creating calculation from INVALID input string"){
    GIVEN("a faulty input string") {
        MathExpression myExp("(1*2)KattenFindus(3+56)");

        THEN("string should be invalid") {
            REQUIRE_FALSE(myExp.isValid());
        }
        THEN(myExp.errorMessage());
        }

    }
